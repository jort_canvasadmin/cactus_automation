package Tests;

import Pages.AuthPage;
import Pages.Header;
import Pages.MainPage;
import Pages.PersonalCabinet;
import org.testng.Assert;
import org.testng.annotations.Test;

public class AccountTest extends BaseTest {

    @Test(dataProvider = "getEmail")
    public void registerTest(String email) {
        AuthPage authPage = new MainPage(driver)
                .getAuthPage()
                .enterRegistrationEmail(email)
                .clickRegistrationButton()
                .fillFields()
                .clickButton();
        Assert.assertTrue(authPage.successMessage.isDisplayed(), "Ваш аккаунт был создан");
    }

    @Test
    public void loginTest() {
        PersonalCabinet personalCabinet = new MainPage(driver)
                .clickToLogin()
                .fillFields("testuser")
                .clickLoginButton();
        Assert.assertTrue(personalCabinet.logout.isDisplayed());
    }

    @Test
    public void logout() {
        PersonalCabinet personalCabinet = new Header(driver)
                .clickToLogin()
                .fillFields("testuser")
                .clickLoginButton();
        Assert.assertTrue(personalCabinet.logout.isDisplayed());
        AuthPage authPage = new Header(driver)
                .logout();
        Assert.assertTrue(authPage.loginButton.isDisplayed());
    }

    @Test
    public void failedLogin() {
        AuthPage authPage = new Header(driver)
                .clickToLogin()
                .fillFields("regvfyhug")
                .clickToFailedLoginButton();
        Assert.assertTrue(authPage.failedLogin.isDisplayed());
    }


}



